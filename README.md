# OpenKarotz Music Player

## Description

This small script will read the **Music** folder at root of your USB stick. It adds a configuration to launch itself at boot automatically, after one minute.

The files and folders placed in **Music** will be played randomly. Folders will be read first and only files later. The folders content is played alphabetically, this allow you to chose if you want to play files alphabetically or not.

## Installation

- On an USB stick, create a root **Music** folder
- Plug the USB *then* turn on your rabbit
- login on telnet following [this tutorial (fr)](http://www.openkarotz.org/administration-avancee/)
- write `touch player.py` then `vi player.py`
- hit the **i** letter on your keybord then copy/paste [player.py](https://code.influence-pc.fr/root/openkarotz-music-player/raw/master/player.py)
- hit the **Escape** key, write `wq!` and hit **Enter**
- Now, just write `python player.py` and enjoy your music!

## Possible improvements

You can easily setup an alarm clock playing with crons:

    # Fall asleep at 21:00 each day
    0 21 * * * /usr/www/cgi-bin/sleep
    # Wake up at 7:00 from Monday to Friday
    0 7 * * 1-5 /usr/www/cgi-bin/wakeup
    # Wake up at 8:00 on Saturday
    0 8 * * 6 /usr/www/cgi-bin/wakeup
    # Wake up at 10:00 on Sunday
    0 10 * * 0 /usr/www/cgi-bin/wakeup

## Limitations

Subfolders are not supported, I think it's not a necessary feature.

While playing a file, the `top` command says that the CPU is used around 85%. You may experience sound lag in certain conditions (not even).

## License

This is a free software placed under the [CeCILL-B license](http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html), inspired from BSD 2-Clause license adapted to French law. It basically allows you to use, share, modify and redistribute this project, including commercial use, without any constraint, at condition to quote me by a link referring to the project page when you will publish your derivative work. I hope you enjoy it :)
