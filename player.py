#!/usr/bin/python -O

import os, random, subprocess, threading

music_folder = "/mnt/usbkey/Music/"
cron_content = "* * * * * /usr/bin/python /usr/karotz/player.py >/dev/null 2>&1"
cron_file = open("/usr/spool/cron/crontabs/root", "r+a")
pid_file = "/tmp/openkarotz_player_launched"

# Makes script launching at boot. OpenKarotz uses busybox,
# where cron daemon doesn't support the @reboot directive
# Append a new line, doesn't erase your exiting content :)
if not cron_content in cron_file.read():
  cron_file.write(cron_content).close()

# The cron calls this script every minute,
# that ensure it is only running one time.
# All files in /tmp/ are cleared at boot.
if not os.path.isfile(pid_file):
  open(pid_file, "w").close()

  if os.path.isfile(music_folder + "playlist.m3u"):
    os.remove(music_folder + "playlist.m3u")

  root_folders = []
  root_files = []
  for (dirpath, dirname, filename) in os.walk(music_folder):
      root_folders.extend(dirname)
      root_files.extend(filename)
      break # only first level

  random.shuffle(root_folders)
  random.shuffle(root_files)

  links = "#EXTM3U\r\n"
  for folder_name in root_folders:
    for item in os.listdir(music_folder + folder_name):
      if os.path.isfile(music_folder + folder_name + "/" + item):
        links += music_folder + folder_name + "/" + item + "\r\n"
  for file_name in root_files:
    links += music_folder + file_name + "\r\n"

  playlist = open(music_folder + "playlist.m3u", "w")
  playlist.write(links)
  playlist.close()

  subprocess.Popen(
    ["mplayer", "-playlist", "playlist.m3u"],
    cwd = music_folder
  )
